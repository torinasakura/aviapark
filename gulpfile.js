/**
 * Created by Andrew Ghostuhin on 06.05.15.
 */
var rimraf = require('rimraf'),
    browserSync = require('browser-sync'),
    browserify = require('browserify'),
    pngquant = require('imagemin-pngquant'),

    argv = require('yargs').argv,

    gulp = require('gulp'),
    gutil = require('gulp-util'),
    jade = require('gulp-jade'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    stylus = require('gulp-stylus'),
    concat = require('gulp-concat'),
    insert = require('gulp-insert'),
    imagemin = require('gulp-imagemin'),
    autoprefixer = require('gulp-autoprefixer'),
    svgstore = require('gulp-svgstore'),


    AppName = argv.name ? argv.name : 'avp',
    package = require('./package.json'),

    reload = browserSync.reload;


var path = {
    avp: {
        jade: './'+AppName+'-app/',
        js: './'+AppName+'-app/js/',
        stylus: './'+AppName+'-app/css/',
        fonts: './'+AppName+'-app/fonts/',
        svg: './'+AppName+'-app/svg/',
        img: './'+AppName+'-app/img/'
    },
    src: {
        jade: ['./src/'+AppName+'/jade/*.jade', '!./src/'+AppName+'/jade/_*.jade'],
        js: ['./node_modules/jquery/dist/jquery.js',
            './node_modules/underscore/underscore.js',
            './node_modules/backbone/backbone.js',
            './node_modules/backbone.marionette/lib/backbone.marionette.js',
            './src/'+AppName+'/vendor/js/slick.js',
            './src/'+AppName+'/js/**/*.js',
            './src/'+AppName+'/map/js/*.js'],
        stylus: './src/'+AppName+'/stylus/app.styl',
        fonts: ['./src/'+AppName+'/fonts/*.*', './src/'+AppName+'/vendor/fonts/**/*.*'],
        svg: './src/'+AppName+'/svg/*.svg',
        img: './src/'+AppName+'/img/**/*.*'
    },
    watch: {
        jade: ['./src/'+AppName+'/jade/*.jade', './src/'+AppName+'/jade/**/*.jade', './src/'+AppName+'/jade/**/_*.jade' ],
        js: ['./src/'+AppName+'/vendor/js/slick.js', './src/'+AppName+'/js/**/*.js'],
        stylus: './src/'+AppName+'/stylus/**/*.styl',
        fonts: ['./src/'+AppName+'/fonts/*.*', './src/'+AppName+'/vendor/fonts/**/*.*'],
        img: './src/'+AppName+'/img/**/*.*'
    },
    destroy: './avp-app'
};

var banner = function() {
    return '/*! '+package.name+' - v'+package.version+' - '+gutil.date(new Date(), "yyyy-mm-dd")+
        ' [copyright: '+package.copyright+']'+' */';
};

var config = {
    server: {baseDir: "./avp-app"},
    watchOptions: {debounceDelay: 1000},
    tunnel: true,
    host: 'localhost',
    port: 9999,
    logPrefix: "AVPScream:"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('destroy', function (cb) {
    rimraf(path.destroy, cb);
});

/* ---------------------------------- */
/* --------- BEGIN AVP:SVG ---------- */
/* ---------------------------------- */
// TODO: write minification for production
gulp.task('svg:avp', function () {
    gulp.src(path.src.svg)
        .pipe(svgstore())
        .pipe(insert.prepend(banner()+'\n'))
        .pipe(gulp.dest(path.avp.svg))
        .pipe(reload({stream:true}))
        .on('error', gutil.log);
});
/* ---------------------------------- */
/* --------- END AVP:SVG ------------ */
/* ---------------------------------- */

/* ---------------------------------- */
/* --------- BEGIN AVP:STYLUS ------- */
/* ---------------------------------- */
gulp.task('stylus:avp', function() {
    gulp.src(path.src.stylus)
        .pipe(stylus({compress: true}))
        .pipe(autoprefixer('last 2 versions', '> 1%', 'ie 9'))
        .pipe(insert.prepend(banner()+'\n'))
        .pipe(insert.prepend('@charset "UTF-8";\n'))
        .pipe(gulp.dest(path.avp.stylus))
        .pipe(reload({stream:true}))
        .on('error', gutil.log);
});
/* -------------------------------- */
/* --------- END AVP:STYLUS ------- */
/* -------------------------------- */

/* -------------------------------- */
/* --------- BEGIN AVP:JADE ------- */
/* -------------------------------- */
gulp.task('jade:avp', function() {
    gulp.src(path.src.jade)
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest(path.avp.jade))
        .pipe(reload({stream:true}))
        .on('error', gutil.log);
});
/* -------------------------------- */
/* --------- END AVP:JADE --------- */
/* -------------------------------- */

/* -------------------------------- */
/* --------- BEGIN AVP:JS --------- */
/* -------------------------------- */
gulp.task('js:avp', function() {
    gulp.src(path.src.js)
        .pipe(sourcemaps.init())
        .pipe(insert.prepend(banner()+'\n'))
        .pipe(uglify({preserveComments:'some'}))
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write('../js', {addComment: false}))
        .pipe(gulp.dest(path.avp.js))
        .pipe(reload({stream:true}))
        .on('error', gutil.log);
});
/* -------------------------------- */
/* --------- END AVP:JS ----------- */
/* -------------------------------- */

gulp.task('img:avp', function() {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.avp.img))
        .pipe(reload({stream: true}))
        .on('error', gutil.log);
});

gulp.task('fonts:avp', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.avp.fonts))
        .pipe(reload({stream: true}))
        .on('error', gutil.log);
});

gulp.task('avp', [
    'jade:avp',
    'js:avp',
    'stylus:avp',
    'fonts:avp',
    'svg:avp',
    'img:avp'
]);


gulp.task('watch', function(){
    gulp.watch([path.watch.jade], function(event, cb) {
        gulp.start('jade:avp');
    });
    gulp.watch([path.watch.stylus], function(event, cb) {
        gulp.start('stylus:avp');
    });
    gulp.watch([path.watch.js], function(event, cb) {
        gulp.start('js:avp');
    });
    gulp.watch([path.watch.img], function(event, cb) {
        gulp.start('img:avp');
    });
    gulp.watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:avp');
    });
});

gulp.task('server', ['avp', 'webserver', 'watch']);
