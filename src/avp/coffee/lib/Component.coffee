React = require "react"
Class = require "../../lib/Class"

class Component extends Class
  @keyBlacklist = '__super__ __superConstructor__ constructor keyBlacklist toComponent'.split ' '

  @toComponent: (componentClass=this, ignore=[]) ->
    component = React.createClass @extractComponentMethods componentClass, ignore
    component.Class = @
    component

  @extractComponentMethods = (comp, ignore, translationDomain) ->
    methods = extractInto {}, (new comp), ignore
    handleMixins methods
    methods.statics = extractInto Class:comp, comp, ignore.concat(['mixins', 'propTypes'])
    methods

extractInto = (target, source, ignore) ->
  for key, val of source
    continue if key in Component.keyBlacklist
    continue if key in ignore
    target[key]= val
  target

extractAndMerge = (prop, merge) ->
  (instance, statics) ->
    result= statics[prop]?() or statics[prop]
    if result?
      unless instance[prop]?
        instance[prop]= result
      else
        instance[prop]= merge(instance[prop], result)

handleMixins = (methods) ->
  if methods.mixins
    unless ClassNameMixin in methods.mixins
      methods.mixins.push ClassNameMixin
  else
    methods.mixins = [ClassNameMixin]

module.exports = Component
