React = require "react/addons"
cx = React.addons.classSet
LinkedValueUtils = require "react/lib/LinkedValueUtils"


ClassNameMixin =
  getMods: ->
    mods = if typeof @props.mods is "string"
      @props.mods.split " "
    else if Array.isArray @props.mods
      @props.mods
    else
      []

    if @modsMap
      for mod, match of @modsMap
        if match is "value"
          mods.push mod if LinkedValueUtils.getValue(@) or LinkedValueUtils.getChecked @
        else if typeof match is "function"
          mods.push mod if match.call @
        else if (@state and @state[match]) or @props[match]
          mods.push mod

    mods

  getClasses: (target, defaults=[]) ->
    className = target.CLASS_NAME or target.constructor.displayName or target.constructor.name
    return defaults if className in ["UI", "Component", "Class"]
    defaults.push className
    parent = target.constructor.Class?.__super__ or target.constructor.__super__
    if parent
      @getClasses parent, defaults
    else
      defaults

  getClassName: (ClassName) ->
    classMap = {}
    classMap[@props.className] = true if @props.className
    mods = @getMods()

    @getClasses(@).forEach (ClassName) ->
      classMap[ClassName] = true
      mods.forEach (mod) ->
        className = [ClassName, mod].join "--"
        classMap[className] = true

    cx classMap

  getElClassName: (element) ->
    classMap = {}
    mods = @getMods()

    @getClasses(@).forEach (ClassName) ->
      ElementClassName = "#{ClassName}-#{element}"
      classMap[ElementClassName] = true
      mods.forEach (mod) ->
        className = [ElementClassName, mod].join "--"
        classMap[className] = true

    cx classMap


module.exports = ClassNameMixin
