LocalCollection = require "../lib/collection/LocalCollection"


collectionState =
  COLLECTION_BEFORE_LOAD: "before_sync"
  COLLECTION_LOADED: "sync"
  COLLECTION_LOAD: "request"
  COLLECTION_DESTROY: "destroy"

CollectionMixin =
  componentWillMount: ->
    return unless @props.collection
    if typeof(@props.collection) is "function"
      collection = new @props.collection()
    else
      collection = @props.collection

    state = collection: collection
    if collection.isLocal or (collection.isNew and collection.isNew()) or collection.length > 0
      state.collectionState = collectionState.COLLECTION_LOADED

    @setState state

    for state, event of collectionState
      collection.on event, @createCollectionStateHandler event

    collection.on "change", @forceUpdateHandler

  componentDidMount: ->
    return unless @props.preload
    return if @state.collection instanceof LocalCollection

    if @state.collection.length is 0
      @state.collection.fetch() unless @state.collection.isNew()

  componentWillUnmount: ->
    return unless @state?.collection
    for state, event of collectionState
      @state.collection.off event
    @state.collection.off "change", @forceUpdateHandler

  componentDidUpdate: (props) ->
    if @props.collection isnt props.collection
      collection = @props.collection
      @setState
        collection: collection
        collectionState: null

      for state, event of collectionState
        collection.on event, @createCollectionStateHandler event

      collection.on "change", @forceUpdateHandler

  createCollectionStateHandler: (state) ->
    (model, resp, options = {}) =>
      return unless @isMounted()
      return if options.actionMethod in ["create", "update"]
      if state is collectionState.COLLECTION_DESTROY
        state = collectionState.COLLECTION_LOADED
      @setState collectionState: state

  forceUpdateHandler: ->
    @forceUpdate() if @isMounted()

  isCollectionLoaded: ->
    @state.collectionState is collectionState.COLLECTION_LOADED

  isCollectionLoad: ->
    @state.collectionState is collectionState.COLLECTION_LOAD

  isCollectionShouldClear: ->
    @state.collectionState is collectionState.COLLECTION_BEFORE_LOAD \
      and @state.collection.length > 0

  loadCollection: ->
    return if @state.collection.isLocal
    return if @isCollectionLoad()
    @state.collection.fetch()

  filterCollectionBy: (filters = {}) ->
    return if @isCollectionLoad()
    @state.collection.filterBy filters

  isCurrentPage: (page) ->
    @state.collection.getCurrentPage() is page

  getPageSize: ->
    @state.collection.getPageSize()

  setPageSize: (pageSize) ->
    @state.collection.setPageSize pageSize

  getPages: ->
    @state.collection.getPages()

  getPagesCount: ->
    @state.collection.getPagesCount()

  loadPage: (page) ->
    return if @isCollectionLoad()
    @state.collection.loadPage page

  clearCollectionFilters: ->
    @state.collection.clearFilters()

  addFilterToCollection: (type, field, value, predicate) ->
    @state.collection.addFilter type, field, value, predicate

  executeCollectionFilters: ->
    @state.collection.executeFilters()


module.exports = CollectionMixin
