CollectionSelectionMixin =
  SELECTION_SINGLE: "single"
  SELECTION_MULTIPLE: "multiple"

  getDefaultProps: ->
    selection: @SELECTION_MULTIPLE

  componentDidMount: ->
    @props.collection.on "select", @handleSelection

  componentWillUnmount: ->
    @props.collection.off "select", @handleSelection

  handleSelection: (model, options = {}) ->
    if @props.selection is @SELECTION_SINGLE
      @props.collection.forEach (item) ->
        item.unselect silent: true if model isnt item
    else if @props.selection is @SELECTION_MULTIPLE
      if options.range and @lastSelection
        currentIndex = @props.collection.indexOf model
        lastIndex = @props.collection.indexOf @lastSelection
        items = if currentIndex > lastIndex
          @props.collection.models.slice lastIndex, currentIndex + 1
        else
          @props.collection.models.slice currentIndex, lastIndex + 1
        items.forEach (item) ->
          if model.isSelected()
            item.select silent: true
          else
            item.unselect silent: true
      @lastSelection = model

    @forceUpdate()

  createSelectionHandler: (model) ->
    (event) =>
      if model.isSelected()
        model.unselect range: event.shiftKey
      else
        model.select range: event.shiftKey

module.exports = CollectionSelectionMixin
