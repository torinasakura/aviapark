dom = require "dom"

ColumnStrategyMixin =
  componentDidMount: ->
    @calculateColumns() if @isMounted()
    @props.columns?.on "change", @handleUpdateColumns
    @addResizeListener @getDOMNode(), @handleCalculateColumns

  componentDidUpdate: (props, state) ->
    @calculateColumns() if @state.width isnt state.width

  componentWillUnmount: ->
    @props.columns?.removeListener "change", @handleUpdateColumns
    @removeResizeListener @getDOMNode(), @handleCalculateColumns

  handleUpdateColumns: ->
    @forceUpdate() if @isMounted()

  handleCalculateColumns: ->
    @calculateColumns()

  calculateColumns: ->
    return unless @state.width > 0
    width = dom(@getDOMNode()).parent().contentWidth()
    @props.columns.calculate width - @props.columns.items.length
    @setState inWidth: width

  reconfigure: ->
    @calculateColumns()

module.exports = ColumnStrategyMixin
