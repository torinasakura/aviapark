ControllableMixin =
  componentWillMount: ->
    ControllerInstance = require @controller
    controller = new ControllerInstance()
    controller.view = @


module.exports = ControllableMixin
