stylesCreated = false
animation = false
animationstring = "animation"
keyframeprefix = ""
animationstartevent = "animationstart"
domPrefixes = "Webkit Moz O ms".split(" ")
startEvents = "webkitAnimationStart animationstart oAnimationStart MSAnimationStart".split(" ")
pfx = ""
elm = document.createElement("fakeelement")
animation = true  if elm.style.animationName isnt `undefined`
if animation is false
  i = 0

  while i < domPrefixes.length
    if elm.style[domPrefixes[i] + "AnimationName"] isnt `undefined`
      pfx = domPrefixes[i]
      animationstring = pfx + "Animation"
      keyframeprefix = "-" + pfx.toLowerCase() + "-"
      animationstartevent = startEvents[i]
      animation = true
      break
    i++

animationName = "resizeanim"
animationKeyframes = "@" + keyframeprefix + "keyframes " + animationName + " { from { opacity: 0; } to { opacity: 0; } } "
animationStyle = keyframeprefix + "animation: 1ms " + animationName + "; "

createStyles = ->
  return if stylesCreated

  css = ((if animationKeyframes then animationKeyframes else "")) + ".resize-triggers { " + ((if animationStyle then animationStyle else "")) + "visibility: hidden; opacity: 0; } " + ".resize-triggers, .resize-triggers > div, .contract-trigger:before { content: \" \"; display: block; position: absolute; top: 0; left: 0; height: 100%; width: 100%; overflow: hidden; } .resize-triggers > div { background: #eee; overflow: auto; } .contract-trigger:before { width: 200%; height: 200%; }"
  head = document.head or document.getElementsByTagName("head")[0]
  style = document.createElement("style")
  style.type = "text/css"
  if style.styleSheet
    style.styleSheet.cssText = css
  else
    style.appendChild document.createTextNode(css)
  head.appendChild style
  stylesCreated = true

resetTriggers = (element) ->
  triggers = element.__resizeTriggers__
  expand = triggers.firstElementChild
  contract = triggers.lastElementChild
  expandChild = expand.firstElementChild
  contract.scrollLeft = contract.scrollWidth
  contract.scrollTop = contract.scrollHeight
  expandChild.style.width = expand.offsetWidth + 1 + "px"
  expandChild.style.height = expand.offsetHeight + 1 + "px"
  expand.scrollLeft = expand.scrollWidth
  expand.scrollTop = expand.scrollHeight

checkTriggers = (element) ->
  element.offsetWidth isnt element.__resizeLast__.width or element.offsetHeight isnt element.__resizeLast__.height

scrollListener = (e) ->
  element = this
  resetTriggers this
  cancelFrame @__resizeRAF__  if @__resizeRAF__
  @__resizeRAF__ = requestFrame ->
    if checkTriggers(element)
      element.__resizeLast__.width = element.offsetWidth
      element.__resizeLast__.height = element.offsetHeight
      element.__resizeListeners__.forEach (fn) ->
        fn.call element, e

requestFrame = (->
  raf = window.requestAnimationFrame or window.mozRequestAnimationFrame or window.webkitRequestAnimationFrame or (fn) ->
    window.setTimeout fn, 20

  (fn) ->
    raf fn
)()

cancelFrame = (->
  cancel = window.cancelAnimationFrame or window.mozCancelAnimationFrame or window.webkitCancelAnimationFrame or window.clearTimeout
  (id) -> cancel id
)()


ElementResizeMixin =
  addResizeListener: (element, fn) ->
    unless element.__resizeTriggers__
      element.style.position = "relative"  if getComputedStyle(element).position is "static"
      createStyles()
      element.__resizeLast__ = {}
      element.__resizeListeners__ = []
      (element.__resizeTriggers__ = document.createElement("div")).className = "resize-triggers"
      element.__resizeTriggers__.innerHTML = "<div class=\"expand-trigger\"><div></div></div>" + "<div class=\"contract-trigger\"></div>"
      element.appendChild element.__resizeTriggers__
      resetTriggers element
      element.addEventListener "scroll", scrollListener, true

      animationstartevent and element.__resizeTriggers__.addEventListener(animationstartevent, (e) ->
        resetTriggers element  if e.animationName is animationName
        return
      )
    element.__resizeListeners__.push fn

  removeResizeListener: (element, fn) ->
    element.__resizeListeners__.splice element.__resizeListeners__.indexOf(fn), 1
    unless element.__resizeListeners__.length
      element.removeEventListener "scroll", scrollListener
      element.__resizeTriggers__ = not element.removeChild(element.__resizeTriggers__)

module.exports = ElementResizeMixin
