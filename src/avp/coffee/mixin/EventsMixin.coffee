access = require "safe-access"

EventsMixin =
  getEventTarget: (path) ->
    access @props, path

  createEventHandler: (handler) ->
    =>
      if handler.slice(-2) is "()"
        context = if access @props, handler.replace("()", "") then @props else @
        access context, handler
      else
        context = if access @props, handler then @props else @
        args = Array.prototype.slice.call arguments
        access context, "#{handler}()", args

  componentDidMount: ->
    return unless @events
    for event, handler of @events
      [path, name] = event.split ":"
      target = @getEventTarget path
      target?.on name, @createEventHandler(handler), @

  componentWillUnmount: ->
    handlers = @_eventHandlers
    for handler in handlers
      handler.off.call @

    return unless @events
    for event, handler of @events
      [path, name] = event.split ":"
      target = @getEventTarget path
      target?.off name, null, @

  handleForceUpdate: ->
    @forceUpdate() if @isMounted()

  splitter: /^([^:]+):?(.*)/

  getInitialState: ->
    handlers = @_eventHandlers = []
    if @events
      for event of @events
        handler = @createHandler event, @events[event]
        handlers.push handler
    null

  trigger: ->
    args = Array.prototype.slice.call arguments, 0
    event = args[0]
    eventListeners = @_eventListeners or {}
    trigger = eventListeners[event]
    params = args.slice 1, args.length
    trigger.apply @, params  if trigger

  createTriggerHandler: ->
    args = Array.prototype.slice.call arguments, 0
    params = args.slice 0, args.length - 1
    callbackParams = args.slice 1, args.length - 1
    callback = args[args.length - 1]
    =>
      @trigger.apply @, params
      callback.apply @, callbackParams if typeof callback is "function"

  createHandler: (event, callback) ->
    if typeof callback is "string"
      callback = @[callback]

    parts = event.match @splitter
    eventName = parts[2]
    path = parts[1]

    @handler path, eventName, callback

  handler: (path, event, callback) ->
    on: ->
      target = @refs[path]
      if target
        eventListeners = target._eventListeners = {}
        eventListeners[event] = callback unless eventListeners[event]
    off: ->
      target = @refs[path]
      if target
        eventListeners = target._eventListeners
        delete eventListeners[event] if eventListeners

module.exports = EventsMixin
