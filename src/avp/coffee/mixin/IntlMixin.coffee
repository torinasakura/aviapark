_ = require "underscore"
IntlManager = require "AVP/service/IntlManager"

IntlMixin =
  getMessage: (message) ->
    IntlManager.get message

  intlMessage: (message, values, TRANSLATION_DOMAIN) ->
    return unless message
    message = message.replace "@", "#{TRANSLATION_DOMAIN or @TRANSLATION_DOMAIN}."
    IntlManager.intlMessage message, values

  formatDate: (date) ->
    IntlManager.formatDate date

  formatTime: (date) ->
    IntlManager.formatTime date

module.exports = IntlMixin
