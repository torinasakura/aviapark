React = require "react"
dom = require "dom"
Tether = require "tether/tether"


LayeredComponentMixin =
  getDefaultProps: ->
    attachment: "top left"
    targetAttachment: "bottom left"

  componentDidUpdate: ->
    @_renderLayer()

  componentWillUnmount: ->
    @_unrenderLayer()

  createLayer: (node) ->
    return if @_layer

    target = dom "<div>"
    target.attr "data-parent-reactid", @_rootNodeID
    dom("body").append target

    options =
      element: target[0]
      target: node or @getDOMNode()
      attachment: @state?.attachment or @props.attachment
      targetAttachment: @state?.targetAttachment or @props.targetAttachment

    options.offset = @props.offset if @props.offset
    options.targetOffset = @props.targetOffset if @props.targetOffset

    @_layer = new Tether options

  _renderLayer: ->
    return unless @_layer
    @_layerComponent = React.renderComponent @renderLayer(), @_layer.element
    @_layer.enable()

  _unrenderLayer: ->
    return unless @_layer
    React.unmountComponentAtNode @_layer.element
    dom(@_layer.element).remove()
    @_layer.destroy()



module.exports = LayeredComponentMixin
