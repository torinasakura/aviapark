ReactLink = require "react/lib/ReactLink"
accounting = require "specials"
#IntlManager = require "AVP/service/IntlManager"

LinkedModelMixin =
  linkModel: (key, options = {}, callback) ->
    model = @model or @props.model or @state?.model

    value = if options.format
      IntlManager.intlNumber model.get(key), options.format
    else
      model.get key

    new ReactLink value, (value) ->
      @value = value
      if options.format
        model.set key, accounting.unformat(value, ","), options
      else
        model.set key, value, options
      callback value if callback

module.exports = LinkedModelMixin
