ReactStateSetters = require "react/lib/ReactStateSetters"
ReactLink = require "react/lib/ReactLink"


LinkedStateMixin =
  linkState: (key, callback) ->
    setter = ReactStateSetters.createStateKeySetter @, key

    value = if @state then @state[key] else null
    new ReactLink value, (value) ->
      setter value
      callback value if callback


module.exports = LinkedStateMixin
