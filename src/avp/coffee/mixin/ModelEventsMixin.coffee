ModelEventsMixin =
  componentWillMount: ->
    @props.model.on "change", @handleUpdateModel

  componentWillUnmount: ->
    @props.model.off "change", @handleUpdateModel

  componentWillReceiveProps: (props) ->
    if props.model isnt @props.model
      props.model.on "change", @handleUpdateModel

  handleUpdateModel: ->
    @forceUpdate() if @isMounted()


module.exports = ModelEventsMixin
