dom = require "dom"


ResizableMixin =
  getInitialState: ->
    width: 0
    height: 0

  componentDidMount: ->
    window.addEventListener "resize", @calculateCallback
    @setState @calculateSize()

  componentWillUnmount: ->
    window.removeEventListener "resize", @calculateCallback

  calculateCallback: ->
    @setState @calculateSize()

  getElementOffsets: (element) ->
    styles = window.getComputedStyle element

    height = 0
    for attr in ["paddingTop", "paddingBottom", "marginTop", "marginBottom"]
      height += parseInt styles[attr].replace "px", ""

    width = 0
    for attr in ["paddingRight", "paddingLeft", "marginRight", "marginLeft"]
      width += parseInt styles[attr].replace "px", ""

    width: width
    height: height

  calculateSize: ->
    offsets = @getElementOffsets @getDOMNode()

    width: dom(document.body).innerWidth() - offsets.width
    height: dom(document.body).innerHeight() \
      - dom(@getDOMNode()).offset().top \
      - offsets.height


module.exports = ResizableMixin
