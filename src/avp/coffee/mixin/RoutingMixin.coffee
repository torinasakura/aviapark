Backbone = require "backbone"


RoutingMixin =
  navigate: (route, options) ->
    options = trigger: true unless options
    Backbone.history.navigate route, options

  navigateHandler: (route) ->
    => @navigate route

module.exports = RoutingMixin
