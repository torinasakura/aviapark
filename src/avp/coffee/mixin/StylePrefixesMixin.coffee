Utils = require "AVP/lib/Utils"
PREFIXES = ["Webkit"]

StylePrefixesMixin =
  addPrefixes: (styles = {}) ->
    for name, value of styles
      for prefix in PREFIXES
        styles["#{prefix}#{Utils.String.capitalize name}"] = value
    styles

module.exports = StylePrefixesMixin
