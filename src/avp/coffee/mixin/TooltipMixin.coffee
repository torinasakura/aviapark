TooltipMixin =
  getInitialState: ->
    showTooltip: false

  handleShowTooltip: ->
    return unless @props.tooltip
    @setState showTooltip: true

  handleHideTooltip: ->
    return unless @props.tooltip
    @setState showTooltip: false


module.exports = TooltipMixin
