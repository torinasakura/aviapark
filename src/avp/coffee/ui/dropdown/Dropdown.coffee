React = require "react"
LayeredComponentMixin = require "../mixin/LayeredComponentMixin"
Button = require "../button/Button"
DropdownPicker = require "/picker/DropdownPicker"
DropdownMenu = require "/menu/DropdownMenu"

class Dropdown extends Button.Class
  mixins: [
    LayeredComponentMixin
  ]

  modsMap:
    "Disabled": -> @isDisabled()
    "Autosize": "autosize"
    "Light": "light"

  getDefaultProps: ->
    disabled: false
    picker: true
    items: []

  getInitialState: ->
    showMenu: false

  isDisabled: ->
    @props.disabled or not (@props.onClick or @canRenderMenu())

  canRenderMenu: ->
    hasVisible = false
    @props.items?.map (item) ->
      hasVisible = true unless item.hidden
    hasVisible

  handleClick: ->
    return if @props.disabled
    if @props.onClick
      @props.onClick()
    else
      @handleToggle()

  handleToggle: ->
    return if @props.disabled
    return unless @canRenderMenu()
    @createLayer()
    @setState showMenu: not @state.showMenu

  ###*
  # @style css
  #   border-radius 2px 0 0 2px
  #   border-right white
  #   background transparent
  ###
  renderButton: -> super

  renderPicker: ->
    <DropdownPicker
    combined={not @props.onClick}
    disabled={@isDisabled()}
    onClick={@handleToggle}/>

      renderLayer: ->
        return <noscript/> unless @state.showMenu

        <DropdownMenu
        items={@props.items}
        onClose={@handleToggle}/>

            ###*
            # @style css
            #   &:hover
            #     background component-hover-color
            ###
          render: ->
            return <noscript/> if @props.hidden

            <div className={@getClassName()}>
            {@renderButton()}
            {@renderPicker() if @props.picker}
            </div>

module.exports = Dropdown.toComponent()
