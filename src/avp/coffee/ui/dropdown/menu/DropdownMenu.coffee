React = require "react"
Component = require "../../../lib/Component"

class DropdownPicker extends Component
  modsMap:
    "Disabled": "disabled"
    "Combined": "combined"

  ###*
  # @style css
  #   position relative
  #   float right
  #   display inline-block
  #   width 25px
  #   height 23px
  #   background component-color
  #   border 1px solid border-color-dark
  #   border-radius 0 2px 2px 0
  #   cursor pointer
  #   outline 0
  #
  #   &:after
  #     content " "
  #     position absolute
  #     width 0
  #     height 0
  #     border-left 4px solid transparent
  #     border-right 4px solid transparent
  #     border-top 4px solid border-color-dark
  #     right 9px
  #     top 11px
  #
  #   &:hover
  #     background component-hover-color
  #
  #   &--Combined
  #     background transparent
  #
  #     &:before
  #       content " "
  #       position absolute
  #       left -1px
  #       top 0px
  #       bottom 0px
  #       border-left 1px solid white
  #
  #   &--Disabled
  #     border-color rgba(0, 0, 0, 0.1)
  #
  #     &:after
  #       border-top-color rgba(0, 0, 0, 0.1)
  ###
  render: ->
    <span
    className={@getClassName()}
    onClick={@props.onClick}/>

    module.exports = DropdownPicker.toComponent()
