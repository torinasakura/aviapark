React = require "react"
Component = require "../../../../lib/Component"

class DropdownMenuItem extends Component
  modsMap:
    "Disabled": "disabled"

  handleClick: ->
    @props.onClick?() unless @props.disabled

  ###*
  # @style css
  #   background component-color
  #   padding 8px 30px 8px 20px
  #   text-align left
  #   white-space nowrap
  #   border-bottom 1px solid rgba(91,91,93,0.03)
  #   cursor pointer
  #
  #   &:hover
  #     background-color rgba(91,91,93,0.02)
  #
  #   &:last-child
  #     border-width 0px
  #
  #   &--Disabled
  #     color rgba(0, 0, 0, 0.1)
  ###
  render: ->
    <div
    className={@getClassName()}
    onClick={@handleClick}>
        {@props.children}
    </div>

module.exports = DropdownMenuItem.toComponent()
