React = require "react"
IntlMixin = require "../mixin/IntlMixin"
Component = require "../../lib/Component"
FAIcon = require "../icon/FAIcon"
Button = require "../button/Button"
Close = require "../close/Close"
Toolbar = require "../toolbar/Toolbar"
Title = require "../title/Title"

class Modal extends Component
  mixins: [
    IntlMixin
  ]

  getDefaultProps: ->
    width: 500

  ###*
  # @style css
  #   position absolute
  #   top 7px
  #   right 11px
  ###
  renderClose: ->
    <span className={@getElClassName "Close"}>
    <Close onClick={@props.onClose}/>
    </span>

  renderTitle: ->
    <Title>{@props.title}</Title>

    ###*
    # @style css
    #   position relative
    #   padding 14px 20px 10px 20px
    #   border-bottom 1px solid #e5e5e5
    ###
  renderHeader: ->
    <div className={@getElClassName "Header"}>
    {@renderTitle()}
    {@renderClose()}
    </div>

  ###*
  # @style css
  #   padding 18px 25px
  ###
  renderBody: ->
    <div className={@getElClassName "Body"}>
      {@props.children}
    </div>

  renderToolbar: ->
    <Toolbar mods="BorderedTop" style={width: "auto"}>
    <Button onClick={@props.onSuccess}>
  {@intlMessage "@save"}
    </Button>
      <Button onClick={@props.onClose}>
        {@intlMessage "@cancel"}
      </Button>
    </Toolbar>

  ###*
  # @style css
  #   margin auto
  #   background white
  #   box-shadow 0px 2px 5px rgba(0, 0, 0, 0.5)
  ###
  render: ->
    @transferPropsTo <div className={@getClassName()}
      style={width: @props.width}>
        {@renderHeader()}
        {@renderBody()}
        {@renderToolbar()}
    </div>

    module.exports = Modal.toComponent()
