$(document).ready(function () {
    var modal_accordion = $('#modal-accordion');

    modal_accordion.find('.btn-show_more').click(function(){
        modal_accordion.find('.collapseOne').slideToggle("200");
        modal_accordion.find('.collapseOne').toggleClass('in');
    });


    var modal_masonry = $('#modal-masonry'),
        columns = modal_masonry.width() > 1400 ? 5.2 : modal_masonry.width() > 768 ? 3.08 : modal_masonry.width() > 480 ? 2 : modal_masonry.width() > 480 ? 2 : 1;

    // Initial of isotope vendor
    modal_masonry.isotope({
        itemSelector: '.block-special',
        masonry: {
            columnWidth: modal_masonry.width() / columns,
            gutter: modal_masonry.width() / 107
        }
    });
    // layout Isotope again after all images have loaded
    modal_masonry.imagesLoaded(function () {
        modal_masonry.isotope('layout');
    });

    //nano scroller
    $(".nano").nanoScroller();

    $("#myModal").on("shown.bs.modal", function() {
        $(".nano").nanoScroller();
    });


});