$(document).ready(function () {

// Initialization tooltip boostrap
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

// Initialization dropdown boostrap
    $('.dropdown-share').dropdown();

    var columns = $(window).width() > 1400 ? 5.2 : $(window).width() > 768 ? 3.08 : $(window).width() > 480 ? 2 : $(window).width() > 480 ? 2 : 1,
        $specialMasonry = $('#special-masonry-container'),
        pre_map = $('.pre-map');

//** Isotope
    // Initial of isotope vendor
    $specialMasonry.isotope({
        itemSelector: '.block-special',
        masonry: {
            columnWidth: $specialMasonry.width() / columns,
            gutter: $specialMasonry.width() / 107
        }
    });
    // layout Isotope again after all images have loaded
    $specialMasonry.imagesLoaded(function () {
        $specialMasonry.isotope('layout');
    });


    // filter with checkboxes
    var $inputs = $('#special-filter input').change( function() {
        // map input values to an array
        var values = [];
        $inputs.each( function( i, elem ) {
            var value;
            if ( elem.type === 'checkbox' ) {
                // if checkbox, use value if checked
                value = elem.checked && elem.value;
            }
            if ( value ) {
                //console.log(value);
                values.push( value );
            }
        });
        var filterValue = values.join(', ');
        $specialMasonry.isotope({ filter: filterValue })
    });

    // Unchecker checkboxes
    $(".discount-box-filter-clear").on("click", function (e) {
        var allCheckbox = $( "input[type='checkbox']" );
        allCheckbox.prop({
            checked: false
        });
        $specialMasonry.isotope({ filter: '*' });
        pre_map.hide();
    });

    // tongle between map and masonry elements
    $("#checkbox-map").change( function() {

        if ( this.checked ) {
            $specialMasonry.isotope({ filter: '.dummy'}); // hide all masonry elements
            if ( document.getElementById('map') == undefined ) {
                pre_map.attr('id', 'map');
                Map.init(true, 2);
            }
            pre_map.show();
        } else {
            $specialMasonry.isotope({ filter: '*'}); // show all masonry elements
            pre_map.hide();
        }

    });

//** Isotope end

});
