/**
 * Created by TorinAsakura on 05.06.15.
 */
scrollrefresh = function () {
    $(".nano").nanoScroller({alwaysVisible: true, iOSNativeScrolling: true});
};

setInterval("scrollrefresh()", 550);

function goinfscroll() {
    var elem = $('.content');
    $('.content').scroll(function () {
        if (window.whait != 1) {
            var elem = $('.content');
            if (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) {
                $("#container").infinitescroll("retrieve");
                window.whait = 1
                $(document).ajaxError(function (e, xhr, opt) { //if there is no more pages remove bind
                    if (xhr.status == 404) {
                        $(".content").unbind("scroll");
                        $(".content").scroll(function () {
                            if ($(this).scrollTop() > 150) {
                                $('#back-top').fadeIn();
                            } else {
                                $('#back-top').fadeOut();
                            }
                        });
                        $(window).scroll(function () {
                            if ($(this).scrollTop() > 150) {
                                $('#back-top').fadeIn();
                            } else {
                                $('#back-top').fadeOut();
                            }
                        });
                    }
                });
            }
        }
    });
    if ($(window).width() < 979) {
        $(document).scroll(function () {
            if (window.whait != 1) {
                if ($(document).height() - $(window).height() - $(document).scrollTop() <= 100) {
                    $("#container").infinitescroll("retrieve");
                    window.whait = 1;
                    $(document).ajaxError(function (e, xhr, opt) { //if there is no more pages remove bind
                        if (xhr.status == 404) {
                            $(".content").unbind("scroll");
                            $(document).unbind("scroll");
                            $(document).scroll(function () {
                                if ($(this).scrollTop() > 150) {
                                    $('#back-top').fadeIn();
                                } else {
                                    $('#back-top').fadeOut();
                                }
                            });
                            $(window).scroll(function () {
                                if ($(this).scrollTop() > 150) {
                                    $('#back-top').fadeIn();
                                } else {
                                    $('#back-top').fadeOut();
                                }
                            });
                        }
                    });
                }
            }
        });
    }
    $(".nano").bind("scrollend", function (e) {
        $(document).ajaxError(function (e, xhr, opt) { //if there is no more pages remove bind
            if (xhr.status == 404) {
                $(".nano").unbind("scrollend");
            }
        });
    });
}
goinfscroll();

function initscroll() {
    if ($(window).width() > 979) {
        scrollrefresha = function () {
            $(".nano2").nanoScroller({alwaysVisible: true});
        };
        refreshS = setInterval("scrollrefresha()", 250);
    } else {
        $(".nano2").nanoScroller({stop: true});
    }
}