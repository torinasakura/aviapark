$(document).ready(function () {
    $('#select-shops').selectize({
        options: [
            {
                class: 'А',
                value: "auchan",
                name: "ашан",
                category: 'продукты',
                logo: 'img/brands/1.png',
                floor: '1 этаж',
                selected: '',
                filters: {special: '1', collection: '1', events: '1', news: '1', favourite: ''}},

            {
                class: 'А',
                value: "mediamarket",
                name: "Mediamarket",
                category: 'техника',
                logo: 'img/brands/4.png',
                floor: '1 этаж',
                selected: '',
                filters: {special: ' ', collection: '', events: '1', news: '', favourite: ''}},

            {
                class: 'А',
                value: "obi",
                name: "OBI",
                category: 'стройматериалы',
                logo: 'img/brands/fs5.jpg',
                floor: '2 этаж',
                selected: '',
                filters: {special: '1', collection: '', events: '', news: '', favourite: '1'}},

            {
                class: 'Б',
                value: 'hoff',
                name: 'hoff',
                category: 'посуда',
                logo: 'img/brands/4.png',
                floor: '2 этаж',
                selected: '',
                filters: {special: '1', collection: '1', events: '', news: '', favourite: ''}},

            {
                class: 'Б',
                value: 'mango',
                name: 'Mango',
                category: 'одежда',
                logo: 'img/brands/5.png',
                floor: '3 этаж',
                selected: '',
                filters: {special: ' ', collection: '', events: '', news: '1', favourite: ''}},
            {
                class: 'Б',
                value: 'decathlon',
                name: 'Decathlon',
                category: 'спорттовары',
                logo: 'img/brands/decathlon.png',
                floor: '3 этаж',
                selected: '1',
                filters: {special: ' ', collection: '1', events: '1', news: '1', favourite: ''}
            },
            {
                class: 'Б',
                value: 'mvideo',
                name: 'м.видео',
                category: 'техника',
                logo: 'img/brands/6.jpg',
                floor: '1 этаж',
                selected: '',
                filters: {special: '1', collection: '', events: '1', news: '', favourite: ''}
            },
            {
                class: 'В',
                value: "auchan2",
                name: "ашан",
                category: 'продукты',
                logo: 'img/brands/1.png',
                floor: '1 этаж',
                selected: '',
                filters: {special: '1', collection: '1', events: '1', news: '1', favourite: ''}
            },

            {
                class: 'В',
                value: "mediamarket2",
                name: "Mediamarket",
                category: 'техника',
                logo: 'img/brands/4.png',
                floor: '1 этаж',
                selected: '',
                filters: {special: ' ', collection: '', events: '1', news: '', favourite: ''}
            },

            {
                class: 'В',
                value: "obi2",
                name: "OBI",
                category: 'стройматериалы',
                logo: 'img/brands/fs5.jpg',
                floor: '2 этаж',
                selected: '',
                filters: {special: '1', collection: '', events: '', news: '', favourite: '1'}
            },

            {
                class: 'В',
                value: "uniqlo",
                name: "uniqlo",
                category: 'одежда',
                logo: 'img/brands/fs5.jpg',
                floor: '2 этаж',
                selected: '',
                filters: {special: '1', collection: '', events: '', news: '', favourite: '1'}
            }
        ],
        optgroups: [
            {value: 'А', label: 'А'},
            {value: 'Б', label: 'Б'},
            {value: 'В', label: 'В'},
            {value: 'Г', label: 'Г'}
        ],
        optgroupField: 'class',
        labelField: 'name',
        searchField: ['name'],
        render: {
            option: function(item, escape) {
                return '<div class="select-options ' + (item.selected ? 'select-options-selected' : ' ') + '">' +
                '<div>' +
                '<div class="meta-search-logo ' + (item.filters.favourite ? 'favorite-shop' : ' ') + '"><img src="' + escape(item.logo) + '"/></div>' +
                '<div class="meta-search-header">' +
                '<ul><li>' + escape(item.name) + '</li><li>' + escape(item.category) + '</li></ul>' +
                '</div>' +
                '</div>' +
                '<div>' +
                '<div class="meta-search-floor">' + escape(item.floor) + '</div>' +
                '<div class="meta-search-icons">' +
                '<div class="search-icons-container"><i class="' + (item.filters.special ? 'av-discount' : ' ')  + ' avv-2x avv-color-second-text"></i></div>' +
                '<div class="search-icons-container"><i class="' + (item.filters.collection ? 'av-tag' : ' ') + ' avv-2x avv-color-second-text"></i></div>' +
                '<div class="search-icons-container"><i class="' + (item.filters.events ? 'av-calendar' : ' ') + ' avv-2x avv-color-second-text"></i></div>' +
                '<div class="search-icons-container"><i class="' + (item.filters.news ? 'av-rupor' : ' ') + ' avv-2x avv-color-second-text"></i></div>' +
                '<div class="search-icons-container search-icons-container-favorite"><span class="avv-stack">' +
                '<i class="avv av-circle avv-stack-2x ' + (item.filters.favourite ? 'avv-transparent' : 'avv-transparent') + ' avv-margin"></i>' +
                '<i class="avv av-star avv-stack-1x ' + (item.filters.favourite ? 'avv-dark-background' : 'avv-transparent') + ' "></i>' +
                '</span></div>' +
                '</div>' +
                '</div>' +
                '</div>';
            }
        }
    });

    $( '<div class="select-to-top"><i class="av-icon-chevron-up"></i></div>' ).insertAfter( '.selectize-dropdown-content' );

    $('.select-to-top').click(function(){
        $('.selectize-dropdown-content').animate({ scrollTop: 0 }, 600);
    });
});