$(document).ready(function () {

	// Initialization tooltip boostrap
	$('[data-toggle="tooltip"]').tooltip({
		container: 'body'
	});

	$(document).ready(function () {
		$('.navbar-toggle').click(function () {
			$('body').addClass('navOpened');
		});
		$('#navBackground').click(function () {
			$('body').removeClass('navOpened');
		});
	});

	var affixed = $('.affix'), affixTimer;
	$('.header-001').hover(function () {
		clearTimeout(affixTimer);
		if (affixed.length > 0) {
			affixed.removeClass('affix');
		} else {
			affixed = $('.affix');
			affixed.removeClass('affix');
		}
	});
	$('.header-002').hover(false, function () {
		if (affixed.length > 0) {
			affixTimer = setTimeout(function () {
				affixed.addClass('affix');
				clearTimeout(affixTimer);
			}, 1500);
		}
	});

});