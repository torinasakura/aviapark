$(document).ready(function () {
    $('.sales-autoplay').slick({
        arrows: false,
        slidesToShow: 4,
        slidesToScroll: 2,
        autoplay: true,
        dots: false,
        autoplaySpeed: 4000,
        speed: 700,
        variableWidth: true,
        responsive: [
            {breakpoint: 1400, settings: {slidesToShow: 2,	slidesToScroll: 1}},
            {breakpoint: 992, settings: {slidesToShow: 2, slidesToScroll: 1, centerMode: true}},
            {breakpoint: 768, settings: {slidesToShow: 1, slidesToScroll: 1, centerMode: true}},
            {breakpoint: 480, settings: {slidesToShow: 1, slidesToScroll: 1, dots: true, centerMode: true}}
        ]
    });
    $('.autoplay-social').slick({
        arrows: false,
        dots: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 700,
        responsive: [{breakpoint: 1400,	settings: {slidesToShow: 4, slidesToScroll: 4,	dots: true}},
            {breakpoint: 992, settings: {slidesToShow: 3, slidesToScroll: 3, dots: true}},
            {breakpoint: 768, settings: {slidesToShow: 2, slidesToScroll: 2, dots: true}},
            {breakpoint: 480, settings: {slidesToShow: 2, slidesToScroll: 2, dots: true}}
        ]
    });
    $('.promo-container').slick({
        infinite: true,
        autoplay: true,
        arrows: true,
        dots: false,
        asNavFor: '.promo-container-caption',
        prevArrow: '<div class="tile-brand-slider-left"><i class="av-chevron-left-circle"></i></div>',
        nextArrow: '<div class="tile-brand-slider-right"><i class="av-chevron-right-circle"></i></div>'
    });
    $('.promo-container-caption').slick({
        infinite: true,
        autoplay: true,
        arrows: false,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        dots: true,
        asNavFor: '.promo-container'
    });
    $('.swiper-container').slick({
        infinite: true,
        autoplay: true,
        arrows: false,
        dots: true
    });
    $('.brands-autoplay-slider').slick({
        infinite: true,
        autoplay: true,
        arrows: false,
        variableWidth: false,
        dots: false,
        swipe: true,
        swipeToSlide: true,
        touchMove: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        autoplaySpeed: 4000,
        speed: 700,
        responsive: [{breakpoint: 1400,	settings: {slidesToShow: 4, slidesToScroll: 4,	dots: false}},
            {breakpoint: 992, settings: {slidesToShow: 3, slidesToScroll: 3, dots: false}},
            {breakpoint: 768, settings: {slidesToShow: 2, slidesToScroll: 2, dots: false}},
            {breakpoint: 480, settings: {slidesToShow: 2, slidesToScroll: 2, dots: false}}
        ]
    });
});